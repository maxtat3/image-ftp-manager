package edu.sintez.tablesample0.app.view;

import edu.sintez.tablesample0.app.model.CategoryTableModel;
import edu.sintez.tablesample0.app.model.SQLiteHelper;
import edu.sintez.tablesample0.app.pojo.Category;
import edu.sintez.tablesample0.app.util.Utils;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


/**
 *
 */
public class CategoriesFrame extends JFrame {

	private List<Category> sourceCategories;
	private List<Category> destinationCategories;
	private ListSelectionModel lsm;
//	private AppComponentsListener actionListener = new AppComponentsListener(table);

	private JButton jbtnCancel, jbtnOk;


	public CategoriesFrame() {
		super("Categories");

		sourceCategories = SQLiteHelper.getInstance().readCategories();
		destinationCategories = Utils.cloneCategories(sourceCategories);

		TableModel catTableModel = new CategoryTableModel(destinationCategories);
		JTable table = new JTable(catTableModel);

		// hint massage
		JLabel jlMsg = new JLabel("Changed take effect only if press ENTER key on cell !");
		jlMsg.setFont(new Font("Tahoma", Font.BOLD, 12));
		jlMsg.setForeground(Color.RED);

		// table
		lsm = table.getSelectionModel();
//		lsm.addListSelectionListener(new AppListSelectionListener(lsm));
		lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionModel(lsm);
		table.setRowSelectionInterval(0, 0);
		JScrollPane jscpTable = new JScrollPane(table,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		table.getColumnModel().getColumn(0).setMaxWidth(400);
		table.getColumnModel().getColumn(1).setMaxWidth(100);

		// directions
		JPanel panelDirection = new JPanel();
		panelDirection.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelDirection.setBorder(BorderFactory.createEtchedBorder());

		jbtnCancel = new JButton("Cancel");
		jbtnOk = new JButton("Ok");
		panelDirection.add(jbtnCancel);
		panelDirection.add(jbtnOk);

		buttonsController();

		JPanel panelMain = new JPanel();
		panelMain.setLayout(new BorderLayout());
		panelMain.add(jlMsg, BorderLayout.PAGE_START);
		panelMain.add(jscpTable, BorderLayout.CENTER);
		panelMain.add(panelDirection, BorderLayout.PAGE_END);
		add(panelMain);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //закрытие второго окна, переход к первому
		setPreferredSize(new Dimension(500, 750));
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
	}

	private void buttonsController() {
		jbtnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeFrame();
			}
		});

		jbtnOk.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SQLiteHelper.getInstance().updateCategories(sourceCategories, destinationCategories);
				closeFrame();
			}
		});
	}

	/**
	 * Close this frame.
	 */
	private void closeFrame() {
		setVisible(false);
		dispose();
	}
}
