package edu.sintez.tablesample0.app.view;

import edu.sintez.tablesample0.app.model.SQLiteHelper;
import edu.sintez.tablesample0.app.util.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 */
public class SettingsFrame extends JFrame {

	private final int[] textFieldDim = {135, 25};

	private JTextField jtfFtpHost;
	private JTextField jtfFtpUserLogin;
	private JTextField jtfFtpUserPsw;
	private JTextField jtfLocalPathToCopyImages;
	private JTextField jtfPathToSQLite;
	private JButton btnCancel;
	private JButton btnOk;


	public SettingsFrame() {
		super("Settings");

		// ---------- data for ftp conn
		JPanel panel0 = new JPanel();
		panel0.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel jlHost = new JLabel("FTP host: ");
		jtfFtpHost = new JTextField();
		jtfFtpHost.setPreferredSize(new Dimension(textFieldDim[0], textFieldDim[1]));

		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel jlFtpUserLogin = new JLabel("FTP user login: ");
		jtfFtpUserLogin = new JTextField();
		jtfFtpUserLogin.setPreferredSize(new Dimension(textFieldDim[0], textFieldDim[1]));

		JPanel panel2 = new JPanel();
		panel1.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel jlFtpUserPsw = new JLabel("FTP user password: ");
		jtfFtpUserPsw = new JTextField();
		jtfFtpUserPsw.setPreferredSize(new Dimension(155, textFieldDim[1]));

		JPanel panel3 = new JPanel();
		panel3.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel jlLocalPathToCopyImages = new JLabel("Copy images to: ");
		jtfLocalPathToCopyImages = new JTextField();
		jtfLocalPathToCopyImages.setPreferredSize(new Dimension(230, textFieldDim[1]));

		JPanel panel4 = new JPanel();
		panel4.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel labelPathToSQLite = new JLabel("Path to SQLite: ");
		jtfPathToSQLite = new JTextField();
		jtfPathToSQLite.setPreferredSize(new Dimension(230, textFieldDim[1]));

		panel0.add(jlHost);
		panel0.add(jtfFtpHost);
		panel1.add(jlFtpUserLogin);
		panel1.add(jtfFtpUserLogin);
		panel2.add(jlFtpUserPsw);
		panel2.add(jtfFtpUserPsw);
		panel3.add(jlLocalPathToCopyImages);
		panel3.add(jtfLocalPathToCopyImages);
		panel4.add(labelPathToSQLite);
		panel4.add(jtfPathToSQLite);

		JPanel panelEnterData = new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelEnterData.setBorder(BorderFactory.createEtchedBorder());
		panelEnterData.add(panel0);
		panelEnterData.add(panel1);
		panelEnterData.add(panel2);
		panelEnterData.add(panel3);
		panelEnterData.add(panel4);

		JPanel panelButtons = new JPanel();
		panelButtons.setLayout(new FlowLayout(FlowLayout.CENTER));
		panelButtons.setBorder(BorderFactory.createEtchedBorder());

		btnCancel = new JButton("Cancel");
		btnOk = new JButton("Ok");

		panelButtons.add(btnCancel);
		panelButtons.add(btnOk);

		SQLiteHelper.getInstance().createAndInitSettings();
		readDataFromModel();
		buttonsController();

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(BorderLayout.CENTER, panelEnterData);
		getContentPane().add(BorderLayout.PAGE_END, panelButtons);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //закрытие второго окна, переход к первому
		setPreferredSize(new Dimension(400, 300));
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
	}

	private void readDataFromModel() {
		String ftpHost, ftpUserName, ftpUserPsw, localPathToCopyImages, sqliteDataDb;

		ftpHost = SQLiteHelper.getInstance().readFtpHostInSettings();
		ftpUserName = SQLiteHelper.getInstance().readFtpUserNameInSettings();
		ftpUserPsw = SQLiteHelper.getInstance().readFtpUserPswInSettings();
		localPathToCopyImages = SQLiteHelper.getInstance().readLocalPathToCopyImgsInSettings();
		sqliteDataDb = SQLiteHelper.getInstance().readPathToSqliteDbInSettings();

		if(! Utils.isEmpty(ftpHost)) jtfFtpHost.setText(ftpHost);
		if(! Utils.isEmpty(ftpUserName)) jtfFtpUserLogin.setText(ftpUserName);
		if(! Utils.isEmpty(ftpUserPsw)) jtfFtpUserPsw.setText(ftpUserPsw);
		if(! Utils.isEmpty(localPathToCopyImages)) jtfLocalPathToCopyImages.setText(localPathToCopyImages);
		if(! Utils.isEmpty(sqliteDataDb)) jtfPathToSQLite.setText(sqliteDataDb);
	}

	private void buttonsController() {
		btnCancel.addActionListener(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				closeFrame();
			}
		});

		btnOk.addActionListener(new ActionListener() {
			String ftpHost, ftpUserName, ftpUserPsw, localPathToCopyImages, sqliteDataDb;
			@Override
			public void actionPerformed(ActionEvent e) {
				ftpHost = jtfFtpHost.getText();
				ftpUserName = jtfFtpUserLogin.getText();
				ftpUserPsw = jtfFtpUserPsw.getText();
				localPathToCopyImages = jtfLocalPathToCopyImages.getText();
				sqliteDataDb = jtfPathToSQLite.getText();

				if(! Utils.isEmpty(ftpHost)) SQLiteHelper.getInstance().updateFtpHostInSettings(ftpHost);
				if(! Utils.isEmpty(ftpUserName)) SQLiteHelper.getInstance().updateFtpUserNameInSettings(ftpUserName);
				if(! Utils.isEmpty(ftpUserPsw)) SQLiteHelper.getInstance().updateFtpUserPswInSettings(ftpUserPsw);
				if(! Utils.isEmpty(localPathToCopyImages)) SQLiteHelper.getInstance().updateLocalPathToCopyImgsInSettings(localPathToCopyImages);
				if(! Utils.isEmpty(sqliteDataDb)) SQLiteHelper.getInstance().updatePathToSqliteDbInSettings(sqliteDataDb);
				closeFrame();
			}
		});
	}

	/**
	 * Close this frame.
	 */
	private void closeFrame() {
		setVisible(false);
		dispose();
	}
}
