package edu.sintez.tablesample0.app.view;

import edu.sintez.tablesample0.app.Config;
import edu.sintez.tablesample0.app.controller.*;
import edu.sintez.tablesample0.app.model.AppTableModel;
import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.util.CopyImageUtils;
import edu.sintez.tablesample0.app.util.PreviewImageUtils;
import edu.sintez.tablesample0.app.util.ProductsComparator;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

/**
 *
 */
public class AppFrame extends JFrame implements Observer {

	public static final String BTN_SETTINGS = "btn_settings";
	public static final String BTN_CAT = "btn_categories";
	public static final String JTF_FIND = "jtf_find";
	public static final String TXT_FIND = "Find article";
	public static final String TXT_EMPTY = "";
	private ListSelectionModel lsm;
	private List<Product> products;
	private PreviewImageUtils previewImageUtils = new PreviewImageUtils();
	private ImageIcon icPreview;
	private JLabel jlPreview, jlImageOriginalSize, jlImageExt;
	private static Comparator<Product> pc = ProductsComparator.getComparatorByCategoryName();


	public AppFrame(List<Product> products) {
		super("AppFrame window");
		this.products = products;

		Collections.sort(products, pc);

		TableModel tableModel = new AppTableModel(products);
		JTable table = new JTable(tableModel){
			@Override
			public String getToolTipText(MouseEvent e) {
				String tip = null;
				java.awt.Point p = e.getPoint();
				int rowIndex = rowAtPoint(p);
				int colIndex = columnAtPoint(p);
				try {
					tip = getValueAt(rowIndex, colIndex).toString();
				} catch (RuntimeException e1) {
					//catch null pointer exception if mouse is over an empty line
				}
				return tip;
			}
		};

		AppComponentsListener actionComponentsListener = new AppComponentsListener(table);

		JPanel jpDirection = new JPanel();
		jpDirection.setPreferredSize(new Dimension(700, Config.PREVIEW_RESOLUTION + 10));
		jpDirection.setLayout(new FlowLayout(FlowLayout.LEFT));
		jpDirection.setBorder(BorderFactory.createEtchedBorder());

		JButton jbtnSettings = new JButton("Settings");
		jbtnSettings.setActionCommand(BTN_SETTINGS);
		jbtnSettings.addActionListener(actionComponentsListener);
		jpDirection.add(jbtnSettings);

		JButton jbtnCategories = new JButton("Categories");
		jbtnCategories.setActionCommand(BTN_CAT);
		jbtnCategories.addActionListener(actionComponentsListener);
		jpDirection.add(jbtnCategories);

		JTextField jtfFind = new JTextField();
		jtfFind.setText(TXT_FIND);
		findTextFieldStyle(jtfFind, FindTextStyleState.SET);
		jtfFind.setActionCommand(JTF_FIND);
		jtfFind.addActionListener(actionComponentsListener);
		jtfFind.addFocusListener(new AppFindFocusAdapter());
		jtfFind.setPreferredSize(new Dimension(150, 25));
		jpDirection.add(jtfFind);

		icPreview = new ImageIcon();
		jlPreview = new JLabel(icPreview);
		jpDirection.add(jlPreview);

		jlImageOriginalSize = new JLabel();
		jpDirection.add(jlImageOriginalSize);

		jlImageExt = new JLabel();
		jpDirection.add(jlImageExt);

		lsm = table.getSelectionModel();
		lsm.addListSelectionListener(new AppListSelectionListener(lsm, this));
		lsm.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setSelectionModel(lsm);
		table.setRowSelectionInterval(0, 0);
		table.getColumnModel().getColumn(0).setMaxWidth(39); // id
		table.getColumnModel().getColumn(1).setMaxWidth(83); // article
		table.getColumnModel().getColumn(2).setMaxWidth(190); // cat
		table.getColumnModel().getColumn(3).setMaxWidth(300); // p name
		table.getColumnModel().getColumn(4).setMaxWidth(300); // f name
		table.getColumnModel().getColumn(5).setMaxWidth(230); // img

		CopyImageUtils ciu = new CopyImageUtils();
		final String copyOriginal= "copyOriginalImgKeyPressed";
		KeyStroke key0 = KeyStroke.getKeyStroke(Config.KEY_COPY_IMAGE_ORIGINAL, 0);
		final String copyAndResize = "copyAndResizedImgKeyPressed";
		KeyStroke key1 = KeyStroke.getKeyStroke(Config.KEY_COPY_IMAGE_WITH_RESIZE, 0);
		final String copyAndResizeAndWm = "copyAndResizeAndWmImgKeyPressed";
		KeyStroke key2 = KeyStroke.getKeyStroke(Config.KEY_COPY_IMAGE_WITH_RESIZE_AND_APPLY_WM, 0);
		table.getInputMap(JTable.WHEN_IN_FOCUSED_WINDOW).put(key0, copyOriginal);
		table.getInputMap(JTable.WHEN_IN_FOCUSED_WINDOW).put(key1, copyAndResize);
		table.getInputMap(JTable.WHEN_IN_FOCUSED_WINDOW).put(key2, copyAndResizeAndWm);
		table.getActionMap().put(copyOriginal, new AppCopyOriginalImgKeyAction(products, ciu));
		table.getActionMap().put(copyAndResize, new AppCopyAndResizedImgKeyAction(products, ciu));
		table.getActionMap().put(copyAndResizeAndWm, new AppCopyAndResizeAndWmImgKeyAction(products, ciu));

		JPanel jpTable = new JPanel();
		jpTable.setLayout(new GridLayout()); // gridlayout - make a JTable fill entire available space
		JScrollPane jscp = new JScrollPane(table,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jpTable.add(jscp);

		JPanel jpInfo = new JPanel();
		jpInfo.setLayout(new BoxLayout(jpInfo, BoxLayout.LINE_AXIS));
		String keyCopyOriginal = KeyEvent.getKeyText(Config.KEY_COPY_IMAGE_ORIGINAL);
		String keyCopyResized = KeyEvent.getKeyText(Config.KEY_COPY_IMAGE_WITH_RESIZE);
		String keyCopyResizedWithWm = KeyEvent.getKeyText(Config.KEY_COPY_IMAGE_WITH_RESIZE_AND_APPLY_WM);
		jpInfo.add(new JLabel(
			"Copy: " + keyCopyOriginal +" original " + " | "
				+ keyCopyResizedWithWm + " resized with WM " + " | "
				+ keyCopyResized + " resized "
		));

		JPanel jpMain = new JPanel();
		jpMain.setLayout(new BorderLayout());
		jpMain.add(jpDirection, BorderLayout.PAGE_START);
		jpMain.add(jpTable, BorderLayout.CENTER);
		jpMain.add(jpInfo, BorderLayout.PAGE_END);
		add(jpMain);

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(1200, 700));
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void findTextFieldStyle(JTextField jtf, FindTextStyleState state) {
		if (state == FindTextStyleState.SET) {
			jtf.setForeground(Color.gray);
			jtf.setFont(new Font("Arial", Font.ITALIC + Font.BOLD, 12));
		} else if (state == FindTextStyleState.RESET) {
			jtf.setForeground(Color.BLACK);
			jtf.setFont(null);
		}
	}


	public enum FindTextStyleState {
		SET, RESET
	}

	public List<Product> getProducts() {
		return products;
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg != null) {
			PreviewImageUtils.PreviewImage previewImage = previewImageUtils.displayImage(String.valueOf(arg));
			icPreview = new ImageIcon(previewImage.getBufferedImage());
			jlPreview.setIcon(icPreview);
			jlPreview.setText("");
			jlImageOriginalSize.setText(previewImage.getWidth() + "x" + previewImage.getHeight());
			jlImageExt.setText(previewImage.getImageFileType().getExt());
		} else {
			jlPreview.setIcon(null);
			jlPreview.setFont(new Font("Arial", Font.BOLD, 16));
			jlPreview.setText("No image available");
			jlImageOriginalSize.setText("");
			jlImageExt.setText("");
		}
	}


}
