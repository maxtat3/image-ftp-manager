package edu.sintez.tablesample0.app.model;

import edu.sintez.tablesample0.app.pojo.Category;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;

/**
 *
 */
public class CategoryTableModel implements TableModel{


	private List<Category> categories;

	public CategoryTableModel(List<Category> categories) {
		this.categories = categories;
	}

	// todo - may be change category description to enum


	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public int getRowCount() {
		return categories.size();
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return "Название категории";
			case 1:
				return "Псевдоним";
		}
		return "";
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Category cat = categories.get(rowIndex);
		switch (columnIndex) {
			case 0:
				return cat.getName();
			case 1:
				return cat.getMetaDescription();
		}
		return "";
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//		System.out.println("cat table model set value");

		Category cat = categories.get(rowIndex);
		if (columnIndex == 1) {
			cat.setMetaDescription((String) aValue);
		}
		categories.set(rowIndex, cat);

//		for (Category cat1 : categories) {
//			System.out.println("cat.getMetaDescription() = " + cat1.getMetaDescription());
//		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0:
				return false;
			case 1:
				return true;
		}
		return false;
	}

	@Override
	public void addTableModelListener(TableModelListener l) {

	}

	@Override
	public void removeTableModelListener(TableModelListener l) {

	}
}
