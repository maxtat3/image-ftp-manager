package edu.sintez.tablesample0.app.model;

import edu.sintez.tablesample0.app.Config;
import edu.sintez.tablesample0.app.pojo.Category;
import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.util.Utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * executeQuery - SELECT queries
 * executeUpdate - INSERT, UPDATE, DELETE queries
 */
public class SQLiteHelper {
	public static final String DLC = ", ";
	public static final String QT = "'";
	public static final String DB_NAME = "intcomua-image-app.db";
	public static final String TABLE_OC_PRODUCT = "oc_product"; // productId, article(model), image
	public static final String TABLE_OC_PRODUCT_DESCRIPTION = "oc_product_description"; // name
	public static final String TABLE_OC_PRODUCT_IMAGE = "oc_product_image"; // additional images
	public static final String TABLE_OC_CATEGORY_DESCRIPTION = "oc_category_description";
	public static final String TABLE_OC_PRODUCT_TO_CATEGORY = "oc_product_to_category";
	public static final String TABLE_APP_SETTINGS = "settings";

	public static final String PRODUCT_ID = "product_id";
	public static final String MODEL = "model";
	public static final String IMAGE = "image";
	public static final String NAME = "name";
	public static final String IMAGE_ID = "product_image_id";
	public static final String CATEGORY_ID = "category_id";
	public static final String META_KEYWORD = "meta_keyword"; // use for alias for create part of file name

	public static final String ID = "id";
	public static final String IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES = "is_translations_names_rus_to_eng";
	public static final String FTP_HOST = "ftp_host";
	public static final String FTP_USER_NAME = "ftp_user_name";
	public static final String FTP_USER_PSW = "ftp_user_psw";
	public static final String LOCAL_PATH_TO_COPY_IMAGES = "local_path_to_copy_images";
	public static final String PATH_TO_SQLITE_DB = "path_to_sqlite_db";


	private static SQLiteHelper instance;

	private List<Product> products = new ArrayList<>();


	public static synchronized SQLiteHelper getInstance() {
		if (instance == null) {
			instance = new SQLiteHelper();
		}
		return instance;
	}


	public void readProductsIdModelMasterImage() {
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ResultSet rs = stmt.executeQuery(
					"SELECT " + PRODUCT_ID + DLC + MODEL + DLC + IMAGE + " FROM " + TABLE_OC_PRODUCT
				);
				while (rs.next()) {
					Product p = new Product();
					p.setId(Integer.parseInt(rs.getString(PRODUCT_ID)));
					p.setModel(Integer.parseInt(rs.getString(MODEL)));
					p.setMasterImage(rs.getString(IMAGE));
					products.add(p); // todo - add call
				}
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void readProductsNameAndFileName() {
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ResultSet rs = stmt.executeQuery(
					"SELECT " + PRODUCT_ID + DLC + NAME + DLC + META_KEYWORD + " FROM " + TABLE_OC_PRODUCT_DESCRIPTION
				);
				while (rs.next()) {
					int idInTable = Integer.parseInt(rs.getString(PRODUCT_ID));
					for (Product p : products) {
						if (idInTable == p.getId()) {
							p.setName(rs.getString(NAME));
							p.setFileName(rs.getString(META_KEYWORD));
						}
					}
				}
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void readProductAdditionalImages() {
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ResultSet rs = stmt.executeQuery(
					"SELECT " + PRODUCT_ID + DLC + IMAGE_ID + DLC + IMAGE + " FROM " + TABLE_OC_PRODUCT_IMAGE
						+ " ORDER BY " + IMAGE + " ASC"
				);
				while (rs.next()) {
					int idInTable = Integer.parseInt(rs.getString(PRODUCT_ID));
					for (Product p : products) {
						if (idInTable == p.getId()) {
							p.getAdditionalImages().add(rs.getString(IMAGE));
						}
					}
				}
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void bindCategoryToProduct() {
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ArrayList<Category> categories = new ArrayList<>();
				ResultSet rsCatDesc = stmt.executeQuery(
					"SELECT " + CATEGORY_ID + DLC + NAME + " FROM " + TABLE_OC_CATEGORY_DESCRIPTION
				);
				while (rsCatDesc.next()) {
					int catId = Integer.parseInt(rsCatDesc.getString(CATEGORY_ID));
					String catName = rsCatDesc.getString(NAME);
					categories.add(new Category(catId, catName));
				}

				ResultSet rsProductToCat = stmt.executeQuery(
					"SELECT " + PRODUCT_ID + DLC + CATEGORY_ID + " FROM " + TABLE_OC_PRODUCT_TO_CATEGORY
				);
				while (rsProductToCat.next()) {
					int prIdInTable = rsProductToCat.getInt(PRODUCT_ID);
					int catIdInTable = rsProductToCat.getInt(CATEGORY_ID);

					for (Product p : products) {
						if (prIdInTable == p.getId()) {

							for (Category cat : categories) {
								if (catIdInTable == cat.getId()) {
									p.setCategoryName(cat.getName());
								}
							}

						}
					}
				}

				rsCatDesc.close();
				rsProductToCat.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Set defaults file names for images from products names.
	 * This method must be called only, when app started at first time !
	 * Name spacing for image files:
	 * - simply copied product name (without changing)
	 * - rus -> eng transliteration
	 * <p/>
	 * WARNING 1: This method must be called after all reading methods:
	 * {@link #readProductsIdModelMasterImage}, {@link #readProductAdditionalImages},
	 * {@link #readProductsNameAndFileName}, {@link #bindCategoryToProduct()}.
	 * <p/>
	 * WARNING 2: This method processed more 10 seconds !
	 */
	public boolean setDefaultFileNames() {
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ResultSet rs = stmt.executeQuery(
					"SELECT " + PRODUCT_ID + DLC + NAME + DLC + META_KEYWORD + " FROM " + TABLE_OC_PRODUCT_DESCRIPTION
				);
				while (rs.next()) {
					Product p = new Product();
					p.setId(rs.getInt(PRODUCT_ID));
					p.setName(rs.getString(NAME));
					products.add(p);
				}
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		bindCategoryToProduct();

		List<Category> categories = readCategories();
		String catMeta = "";
		String query;
		stmt = sqlExecutor(SqlAction.PREPARE);

		System.out.println("products size= " + products.size());
		for (Product p : products) {
			String pCatName = p.getCategoryName();
			for (Category cat : categories) {
				if (cat.getName().equals(pCatName)) catMeta = cat.getMetaDescription();
			}
			try {
				query = "UPDATE " + TABLE_OC_PRODUCT_DESCRIPTION
					+ " SET " + META_KEYWORD + "=" + QT + catMeta + Utils.replaceForbiddenSymbols(p.getName()) + QT
					+ " WHERE " + PRODUCT_ID + "=" + p.getId();
				System.out.println("query = " + query);
				if (stmt != null) stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}


	public void mergePreffixAndAliasInImageFileName() {

	}


	/**
	 * Prints next info for each product:
	 * - id
	 * - model
	 * - name
	 * - image file name
	 * - master image
	 * - additional images
	 */
	public void printProducts() {
		for (Product p : products) {
			System.out.println("id=" + p.getId()
				+ "|" + "article=" + p.getModel()
				+ "|" + "name=" + p.getName()
				+ "|" + "ifname" + p.getFileName()
				+ "|" + "img=" + p.getMasterImage()
				+ "|" + "cat=" + p.getCategoryName()
			);

			if (p.getAdditionalImages().size() > 0) {
				for (String img : p.getAdditionalImages()) {
					System.out.println(img);
				}
			}
		}
	}

	/**
	 * Returned list of products.
	 *
	 * @return list of products
	 */
	public List<Product> getProducts() {
		return products;
	}


	// used for set name of image when his copied to destination dir
	public void updateProductFileNames(Product p) {
		String query;
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			query = "UPDATE " + TABLE_OC_PRODUCT_DESCRIPTION
				+ " SET " + META_KEYWORD + "=" + QT + p.getFileName() + QT
				+ " WHERE " + PRODUCT_ID + "=" + p.getId();
			if (stmt != null) stmt.executeUpdate(query);
			sqlExecutor(SqlAction.CLOSE);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Category> readCategories() {
		List<Category> categories = null;
		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		if (stmt != null) {
			try {
				ResultSet rs = stmt.executeQuery(
					"SELECT " + CATEGORY_ID + DLC + NAME + DLC + META_KEYWORD + " FROM " + TABLE_OC_CATEGORY_DESCRIPTION
						+ " ORDER BY " + NAME + " ASC"
				);
				categories = new ArrayList<>();
				while (rs.next()) {
					Category cat = new Category();
					cat.setId(Integer.parseInt(rs.getString(CATEGORY_ID)));
					cat.setName(rs.getString(NAME));
					cat.setMetaDescription(rs.getString(META_KEYWORD));
					categories.add(cat);
				}
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return categories;
	}

	/**
	 * Check only {@link Category#metaDescription} field, because only this field user must be changed.
	 *
	 * @param source      source list when frame is creating and visible
	 * @param destination changed introduced in source list when frame is visible
	 * @see CategoryTableModel#isCellEditable(int, int)
	 */
	public int updateCategories(List<Category> source, List<Category> destination) {
		if (source.size() != destination.size()) return -1;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		String query;
		Category sCat, dCat;

		for (int i = 0; i < source.size(); i++) {
			sCat = source.get(i);
			dCat = destination.get(i);
			if (!sCat.getMetaDescription().equals(dCat.getMetaDescription())) {
				query = "UPDATE " + TABLE_OC_CATEGORY_DESCRIPTION
					+ " SET " + META_KEYWORD + "=" + QT + dCat.getMetaDescription() + QT
					+ " WHERE " + CATEGORY_ID + "=" + dCat.getId();
				try {
					if (stmt != null) stmt.executeUpdate(query);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		sqlExecutor(SqlAction.CLOSE);

		return 1;
	}


	/**
	 * Create and initialize default data settings table in db.
	 * If this table early is created - ignored create and init process.
	 * This method can be safety called every time when frame is created.
	 */
	public void createAndInitSettings() {
		String queryCreate = "CREATE TABLE IF NOT EXISTS " + TABLE_APP_SETTINGS
			+ "("
			+ ID + " INTEGER PRIMARY KEY NOT NULL DEFAULT 1, "
			+ IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES + " INTEGER, "
			+ FTP_HOST + " TEXT, "
			+ FTP_USER_NAME + " TEXT, "
			+ FTP_USER_PSW + " TEXT, "
			+ LOCAL_PATH_TO_COPY_IMAGES + " TEXT, "
			+ PATH_TO_SQLITE_DB + " TEXT"
			+ ")";

		String qIsRowExist = "SELECT * FROM " + TABLE_APP_SETTINGS;

		String queryInsertDefData = "INSERT INTO " + TABLE_APP_SETTINGS
			+ " ("
			+ IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES + DLC
			+ FTP_HOST + DLC
			+ FTP_USER_NAME + DLC
			+ FTP_USER_PSW + DLC
			+ LOCAL_PATH_TO_COPY_IMAGES + DLC
			+ PATH_TO_SQLITE_DB
			+ ") VALUES (" + 0 + DLC
			+ QT + "" + QT + DLC
			+ QT + "" + QT + DLC
			+ QT + "" + QT + DLC
			+ QT + "" + QT + DLC
			+ QT + "" + QT
			+ ");";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.execute(queryCreate);
				ResultSet rs = stmt.executeQuery(qIsRowExist);
				if (!rs.next()) stmt.execute(queryInsertDefData);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public boolean readTranslationsNamesRusToEngInSettings() {
		String query = "SELECT " + IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES + " FROM " + TABLE_APP_SETTINGS;
		int isMake = 0;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) isMake = rs.getInt(IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isMake == 1;
	}

	public void updateTranslationsNamesRusToEngInSettings(boolean isMake) {
		int val = (isMake) ? 1 : 0;
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES + "=" + QT + val + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String readFtpHostInSettings() {
		String query = "SELECT " + FTP_HOST + " FROM " + TABLE_APP_SETTINGS;
		String host = null;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) host = rs.getString(FTP_HOST);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return host;
	}

	public void updateFtpHostInSettings(String host) {
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + FTP_HOST + "=" + QT + host + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String readFtpUserNameInSettings() {
		String query = "SELECT " + FTP_USER_NAME + " FROM " + TABLE_APP_SETTINGS;
		String userName = null;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) userName = rs.getString(FTP_USER_NAME);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userName;
	}

	public void updateFtpUserNameInSettings(String userName) {
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + FTP_USER_NAME + "=" + QT + userName + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String readFtpUserPswInSettings() {
		String query = "SELECT " + FTP_USER_PSW + " FROM " + TABLE_APP_SETTINGS;
		String userPsw = null;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) userPsw = rs.getString(FTP_USER_PSW);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userPsw;
	}

	public void updateFtpUserPswInSettings(String userPsw) {
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + FTP_USER_PSW + "=" + QT + userPsw + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String readLocalPathToCopyImgsInSettings() {
		String query = "SELECT " + LOCAL_PATH_TO_COPY_IMAGES + " FROM " + TABLE_APP_SETTINGS;
		String localPath = null;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) localPath = rs.getString(LOCAL_PATH_TO_COPY_IMAGES);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return localPath;
	}

	public void updateLocalPathToCopyImgsInSettings(String imgsLocalPath) {
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + LOCAL_PATH_TO_COPY_IMAGES + "=" + QT + imgsLocalPath + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String readPathToSqliteDbInSettings() {
		String query = "SELECT " + PATH_TO_SQLITE_DB + " FROM " + TABLE_APP_SETTINGS;
		String userName = null;

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				ResultSet rs = stmt.executeQuery(query);
				while (rs.next()) userName = rs.getString(PATH_TO_SQLITE_DB);
				rs.close();
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userName;
	}

	public void updatePathToSqliteDbInSettings(String dbDataPath) {
		String query = "UPDATE " + TABLE_APP_SETTINGS
			+ " SET " + PATH_TO_SQLITE_DB + "=" + QT + dbDataPath + QT
			+ " WHERE " + ID + "=1";

		Statement stmt = sqlExecutor(SqlAction.PREPARE);
		try {
			if (stmt != null) {
				stmt.executeUpdate(query);
				sqlExecutor(SqlAction.CLOSE);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


//	private int intInSettings(ActionSettings action, int val){
//		int valFromTable = -1;
//		if (action == ActionSettings.READ) {
//			Statement stmt = sqlExecutor(SqlAction.PREPARE);
//			try {
//				if (stmt != null) {
//					ResultSet rs = stmt.executeQuery(query);
//					while (rs.next()) valFromTable = rs.getInt(IS_MAKE_TRANSLATIONS_NAMES_RUS_TO_ENG_P_NAMES);
//					rs.close();
//					sqlExecutor(SqlAction.CLOSE);
//				}
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//
//		} else if (action == ActionSettings.WRITE) {
//
//		}
//		return -1;
//	}
//
//	private enum ActionSettings {
//		READ, WRITE
//	}


	/**
	 * Prepare sql to execute connection.
	 * See available actions in {@link SqlAction}:
	 * {@link SqlAction#PREPARE} - open db and prepare connection,
	 * {@link SqlAction#CLOSE} - close {@link Statement} and {@link Connection} if it are early open
	 *
	 * @param action
	 * @return Statement reference
	 */
	private Statement sqlExecutor(SqlAction action) {
		Connection conn = null;
		Statement stmt = null;

		if (action == SqlAction.PREPARE) {
			try {
				Class.forName("org.sqlite.JDBC");
				conn = DriverManager.getConnection("jdbc:sqlite:" + Config.DB_PATH);
				stmt = conn.createStatement();
				return stmt;
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}

		} else if (action == SqlAction.CLOSE) {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	private enum SqlAction {
		PREPARE, // init and return resultSet object
		CLOSE // close connection
	}

}
