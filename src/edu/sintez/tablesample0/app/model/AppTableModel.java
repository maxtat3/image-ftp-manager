package edu.sintez.tablesample0.app.model;

import edu.sintez.tablesample0.app.pojo.Product;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 *
 */
public class AppTableModel implements TableModel {

	public static final int COLUMNS_SIZE = 6;
	private Set<TableModelListener> listeners = new HashSet<>();
	private List<Product> products = new ArrayList<>(); //todo - remove new

	public AppTableModel(List<Product> products) {
		this.products = products;
	}


	/**
	 * JTable должен знать, какие данные он должен отобразить в какой колонке.
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return Integer.class;
			case 1:
				return Integer.class;
			case 2:
				return String.class;
			case 3:
				return String.class;
			case 4:
				return String.class;
			case 5:
				return String.class;
		}
		return Object.class;
	}

	/**
	 * Возвращает количество столбцов которое будет отображаться в таблице.
	 * В MyBean у нас 3 поля, значит и возвращать у нас метод будет число 3.
	 */
	@Override
	public int getColumnCount() {
		return COLUMNS_SIZE;
	}

	@Override
	public int getRowCount() {
		return products.size();
	}

	/**
	 * возвращает заголовок колонки по её индексу. У нас три колонки.
	 * Внутри метода проверяем индекс и возвращаем соответствующее имя колонки.
	 */
	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return "Id";
			case 1:
				return "Article";
			case 2:
				return "Category";
			case 3:
				return "Product name";
			case 4:
				return "File name";
			case 5:
				return "Image path";
		}
		return "";
	}

	/**
	 * отвечает за то, какие данные в каких ячейках JTable будут показываться.
	 * Методу в качестве параметров передается индекс строки и столбца ячейки JTable.
	 * По индексу строки мы из списка products получаем соответствующую сущность,
	 * а по индексу колонки узнаем данные из какого поля Product необходимо показать.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Product p = products.get(rowIndex);

		switch (columnIndex) { // если применен return то break не нужен
			case 0:
				return p.getId();
			case 1:
				return p.getModel();
			case 2:
				return p.getCategoryName();
			case 3:
				return p.getName();
			case 4:
				return p.getFileName();
			case 5:
				return p.getMasterImage();
		}

		return "";
	}

	/**
	 * Редактирование определенной ячейки.
	 */
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Product p = products.get(rowIndex);
		switch (columnIndex) {
			case 4:
				p.setFileName((String) aValue);
				SQLiteHelper.getInstance().updateProductFileNames(p);
				break;
		}
		products.set(rowIndex, p);
	}

	/**
	 * Редактируемы ли ячейки таблицы? true - Да.
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		System.out.println("editable : row = " + rowIndex + " | " + " column = " + columnIndex);
		switch (columnIndex) {
			case 0:
				return false;
			case 1:
				return false;
			case 2:
				return false;
			case 3:
				return false;
			case 4:
				return true;
			case 5:
				return false;
		}
		return false;
	}

	/**
	 * Добавление слушателей. Они нужны для того, чтобы JTable был в курсе всех модификаций модели.
	 * К прмеру, добавилась в модель новая сущность или удалилась. В этом случае модель TableModel
	 * генерирует специальное событие, в котором содержится необходимая информация о том, что произошло с моделью.
	 * Далее модель пробегается по всем своим слушателям и оповещает их о произошедшем событии.
	 *
	 * @param l слушатель
	 */
	@Override
	public void addTableModelListener(TableModelListener l) {
		listeners.add(l);
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	}

}
