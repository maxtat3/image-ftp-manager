package edu.sintez.tablesample0.app;

import edu.sintez.tablesample0.app.util.Utils;

import java.awt.event.KeyEvent;

/**
 *
 */
public class Config {
	public static final String DB_PATH = Utils.receivePathCurrentDir() + "/" + "intcomua-image-app.db";
//	public static final String TMP_IMAGE_PATH = "/home/max"; // for debug
	public static final String TMP_IMAGE_PATH = Utils.receivePathCurrentDir() + "/"; // for release
	public static final String TMP_IMAGE_FILENAME = "imagetmp";
	public static final String TMP_IMAGE_ABS_PATH = TMP_IMAGE_PATH + "/" + TMP_IMAGE_FILENAME;
	public static final String WATERMARK_PATH = Utils.receivePathCurrentDir() + "/" + "wm.png";
	public static final String DLM = "/";
	public static final int PREVIEW_RESOLUTION = 150; //px
	public static final int RESIZE_IMAGE_RESOLUTION = 700; //px
	public static final int KEY_COPY_IMAGE_ORIGINAL = KeyEvent.VK_F5;
	public static final int KEY_COPY_IMAGE_WITH_RESIZE_AND_APPLY_WM = KeyEvent.VK_F9;
	public static final int KEY_COPY_IMAGE_WITH_RESIZE = KeyEvent.VK_F12;
}
