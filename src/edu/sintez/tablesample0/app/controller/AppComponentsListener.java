package edu.sintez.tablesample0.app.controller;

import edu.sintez.tablesample0.app.util.Utils;
import edu.sintez.tablesample0.app.view.AppFrame;
import edu.sintez.tablesample0.app.view.CategoriesFrame;
import edu.sintez.tablesample0.app.view.SettingsFrame;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 */
public class AppComponentsListener implements ActionListener {

	private JTable table;

	public AppComponentsListener(JTable table) {
		this.table = table;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton btn = (JButton) e.getSource();
			buttonsAction(btn);
		} else if (e.getSource() instanceof JTextField) {
			JTextField tf = (JTextField) e.getSource();
			textFieldsAction(tf);
		}

	}

	private void buttonsAction(JButton btn) {
		if (btn.getActionCommand().equals(AppFrame.BTN_SETTINGS)) {
			System.out.println("btn settings pressed");
			new SettingsFrame();

		} else if (btn.getActionCommand().equals(AppFrame.BTN_CAT)) {
			System.out.println("btn categories pressed");
			new CategoriesFrame();

		}
	}


	// FIXME: 03.03.17  - find everywhere
	private void textFieldsAction(JTextField tf) {
		int row = 0;
		boolean isNum = Utils.isNumber(tf.getText());

		if (isNum) row = getRowByVal(table.getModel(), Integer.parseInt(tf.getText()));
//		else row = getRowByVal(table.getModel(), tf.getText());

		table.setRowSelectionInterval(row, row);
//		table.scrollRectToVisible(table.getCellRect(row, 0, true));
		table.scrollRectToVisible(table.getCellRect(row, 1, true));
	}


	private int getRowByVal(TableModel model, String text) {
		for (int i = model.getRowCount() - 1; i >= 0; --i) {
			for (int j = model.getColumnCount() - 1; j >= 0; --j) {
				if (model.getValueAt(i, j).equals(text)) {
					return i;
				}
			}
		}
		return 0;
	}

	//todo - find everywhere incorrect word - find only in 0 column
//	private int getRowByVal(TableModel model, Integer num) {
//		boolean isNum;
//		String numInTable;
//
//		for (int i = model.getRowCount() - 1; i >= 0 ; --i) {
//			for (int j = model.getColumnCount() - 1; j >= 0 ; --j) {
//				numInTable = String.valueOf(model.getValueAt(i, j));
//				isNum = Utils.isNumber(numInTable);
//				if (isNum && Integer.parseInt(numInTable) == num) {
//					return i;
//				}
//			}
//		}
//		return 0;
//	}

	// find article only
	private int getRowByVal(TableModel model, Integer num) {
		boolean isNum;
		String numInTable;

		for (int i = model.getRowCount() - 1; i >= 0; --i) {
			numInTable = String.valueOf(model.getValueAt(i, 1));
			isNum = Utils.isNumber(numInTable);
			if (isNum && Integer.parseInt(numInTable) == num) {
				return i;
			}
		}
		return 0;
	}


}
