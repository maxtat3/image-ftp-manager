package edu.sintez.tablesample0.app.controller;

import edu.sintez.tablesample0.app.ftp.Ftp;
import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.view.AppFrame;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import java.util.Observable;

/**
 * Change value on cell. Only when cell is editable.
 *
 * @see edu.sintez.tablesample0.app.model.AppTableModel#isCellEditable
 */
public class AppListSelectionListener extends Observable implements ListSelectionListener{
	private ListSelectionModel lsm;
	private List<Product> products;

	public AppListSelectionListener(ListSelectionModel lsm, AppFrame appFrame) {
		this.lsm = lsm;
		this.products = appFrame.getProducts();
		addObserver(appFrame);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		int minIndex = lsm.getMinSelectionIndex();
		int maxIndex = lsm.getMaxSelectionIndex();
		for (int i = minIndex; i <= maxIndex; i++) {
			if (lsm.isSelectedIndex(i)) {
				// receive master image
				String tmpImgPath = Ftp.getInstance().downloadFileFromHostingAndSaveToLocal(
					Ftp.getInstance().getHostingImgPath() + "/" + products.get(i).getMasterImage(), 0
				);
				setChanged();
				notifyObservers(tmpImgPath);

				//receive additional images
				for (int j = 0; j < products.get(i).getAdditionalImages().size(); j++) {
					Ftp.getInstance().downloadFileFromHostingAndSaveToLocal(
						Ftp.getInstance().getHostingImgPath() + "/" + products.get(i).getAdditionalImages().get(j), j+1
					);
				}
			}
		}
	}
}
