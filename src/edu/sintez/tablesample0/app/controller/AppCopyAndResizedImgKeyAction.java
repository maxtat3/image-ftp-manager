package edu.sintez.tablesample0.app.controller;

import edu.sintez.tablesample0.app.Config;
import edu.sintez.tablesample0.app.model.SQLiteHelper;
import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.util.CopyImageUtils;
import edu.sintez.tablesample0.app.util.Utils;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Used when user stay on any row in table and press custom key (now PAGE_UP) in keyboard.
 */
public class AppCopyAndResizedImgKeyAction extends AbstractAction {

	private List<Product> products;
	private CopyImageUtils ciu;

	public AppCopyAndResizedImgKeyAction(List<Product> products, CopyImageUtils ciu) {
		this.products = products;
		this.ciu = ciu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JTable) {
			JTable jt = (JTable) e.getSource();
			int selectedRow = jt.getSelectedRow();

			// solution without class constructor
//			String productName = String.valueOf(jt.getModel().getValueAt(selectedRow, 3)); // todo - 3 changed to value from enum in table model
//			String imageName = String.valueOf(jt.getModel().getValueAt(selectedRow, 5)); // todo - 5 changed to value from enum in table model

//			try {
//				Files.copy(
//					new File(PreviewImageUtils.TMP_IMAGE_ABS_PATH + "." + Utils.getImageExtension(imageName).getExt()).toPath(),
//					new File(SQLiteHelper.getInstance().readLocalPathToCopyImgsInSettings()
//						+ Utils.replaceForbiddenSymbols(productName)
//						+ "." + Utils.getImageExtension(imageName).getExt()).toPath(),
//					StandardCopyOption.COPY_ATTRIBUTES
//				);
//			} catch (FileAlreadyExistsException faee) {
//				System.out.println("this file is already exist");
//				faee.printStackTrace();
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}







//			Product p = products.get(selectedRow);
//			String fileName = p.getFileName();
//			String imageName = p.getMasterImage();
//			ArrayList<String> additionalImages = p.getAdditionalImages();
//
//			copyImageToLocal(imageName, fileName, 0, true); // first image
//			for (int i = 0; i < additionalImages.size(); i++) { // other images
//				copyImageToLocal(additionalImages.get(i), fileName, i + 1, false);
//			}


			Product p = products.get(selectedRow);
			ciu.copyImage(p, CopyImageUtils.CopyImageAction.COPY_WITH_RESIZED);

		}
	}


}
