package edu.sintez.tablesample0.app.controller;

import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.util.CopyImageUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 *
 */
public class AppCopyAndResizeAndWmImgKeyAction extends AbstractAction {
	private List<Product> products;
	private CopyImageUtils ciu;

	public AppCopyAndResizeAndWmImgKeyAction(List<Product> products, CopyImageUtils ciu) {
		this.products = products;
		this.ciu = ciu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JTable) {
			JTable jt = (JTable) e.getSource();
			int selectedRow = jt.getSelectedRow();
			Product p = products.get(selectedRow);
			ciu.copyImage(p, CopyImageUtils.CopyImageAction.COPY_WITH_RESIZED_AND_APPLY_WM);
		}
	}
}
