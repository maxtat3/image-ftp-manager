package edu.sintez.tablesample0.app.controller;

import edu.sintez.tablesample0.app.view.AppFrame;

import javax.swing.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 *
 */
public class AppFindFocusAdapter extends FocusAdapter {

	@Override
	public void focusGained(FocusEvent e) {
		if (!isJtf(e)) return;

		JTextField jtfFind = (JTextField) e.getSource();
		if (jtfFind.getText().equals(AppFrame.TXT_FIND)) {
			jtfFind.setText(AppFrame.TXT_EMPTY);
			AppFrame.findTextFieldStyle(jtfFind, AppFrame.FindTextStyleState.RESET);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (!isJtf(e)) return;

		JTextField jtfFind = (JTextField) e.getSource();
		if (jtfFind.getText().equals(AppFrame.TXT_EMPTY)) {
			jtfFind.setText(AppFrame.TXT_FIND);
			AppFrame.findTextFieldStyle(jtfFind, AppFrame.FindTextStyleState.SET);
		}
	}

	private boolean isJtf(FocusEvent e) {
		return e.getSource() instanceof JTextField;
	}
}
