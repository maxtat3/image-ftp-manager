package edu.sintez.tablesample0.app.util;

import edu.sintez.tablesample0.app.Config;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *
 */
public class PreviewImageUtils {

	public PreviewImage displayImage(String path) {
		File file = new File(path);
		BufferedImage bufImg;
		PreviewImage pv = null;
		try {
			bufImg = ImageIO.read(file);
			int x = bufImg.getWidth();
			int y = bufImg.getHeight();

			int[] resized = resize(x, y, Config.PREVIEW_RESOLUTION, AxisSelector.RESIZE_OF_Y_AXIS);
			int rx = resized[0];
			int ry = resized[1];

			bufImg = createResizedCopy(bufImg, rx, ry, true);
			pv = new PreviewImage(bufImg, x, y, Utils.getImageExtension(file));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (pv != null) return pv;
		return null;
	}

	/**
	 * Change (resize) coordinates for image.
	 * This save ratio of image of x or y coordinates selected in {@link AxisSelector} class.
	 *
	 * @param imgX width in px
	 * @param imgY height in px
	 * @param scale scale image to this value in px
	 * @return array of changed coordinates.
	 * This array contain two elements: 0 elem = x , 1 elem = y.
	 */
	private int[] resize(double imgX, double imgY, int scale, AxisSelector sel) {
		double x = 0, y = 0, k = 0;

		if (sel == AxisSelector.RESIZE_OF_X_AXIS) {
			k = imgX / scale; // in this case kx
			y = imgY / k;
			x = scale;

		} else if (sel == AxisSelector.RESIZE_OF_Y_AXIS) {
			k = imgY / scale; // in this case ky
			x = imgX / k;
			y = scale;
		}

		return new int[]{(int) x, (int) y};
	}

	private enum AxisSelector {
		RESIZE_OF_X_AXIS, // x axis is const, y is change
		RESIZE_OF_Y_AXIS // y axis is const
	}


	/**
	 * Easy resize image method. Not supported smooth processing.
	 *
	 * @param originalImage reference to buffered original image
	 * @param scaledWidth scaled to this width in px
	 * @param scaledHeight scaled to this height in px
	 * @param preserveAlpha is remain alpha chanel (actual for png images)
	 * @return resized image
	 */
	private BufferedImage createResizedCopy(BufferedImage originalImage,
	                                        int scaledWidth, int scaledHeight,
	                                        boolean preserveAlpha) {
		int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
		Graphics2D g = scaledBI.createGraphics();
		if (preserveAlpha) {
			g.setComposite(AlphaComposite.Src);
		}
		g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
		g.dispose();
		return scaledBI;
	}

	public class PreviewImage {
		private BufferedImage bufferedImage;
		private int width, height;
		private Utils.ImageFileType imageFileType;

		public PreviewImage(BufferedImage bufferedImage, int width, int height) {
			this.bufferedImage = bufferedImage;
			this.width = width;
			this.height = height;
		}

		public PreviewImage(BufferedImage bufferedImage, int width, int height, Utils.ImageFileType extension) {
			this.bufferedImage = bufferedImage;
			this.width = width;
			this.height = height;
			this.imageFileType = extension;
		}

		public BufferedImage getBufferedImage() {
			return bufferedImage;
		}

		public void setBufferedImage(BufferedImage bufferedImage) {
			this.bufferedImage = bufferedImage;
		}

		public int getWidth() {
			return width;
		}

		public void setWidth(int width) {
			this.width = width;
		}

		public int getHeight() {
			return height;
		}

		public void setHeight(int height) {
			this.height = height;
		}

		public Utils.ImageFileType getImageFileType() {
			return imageFileType;
		}

		public void setImageFileType(Utils.ImageFileType imageFileType) {
			this.imageFileType = imageFileType;
		}
	}
}
