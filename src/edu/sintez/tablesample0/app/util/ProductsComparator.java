package edu.sintez.tablesample0.app.util;

import edu.sintez.tablesample0.app.pojo.Product;

import java.util.Comparator;

/**
 *
 */
public class ProductsComparator {

	private static ComparatorById comparatorById;
	private static ComparatorByArticle comparatorByArticle;
	private static ComparatorByCategoryName comparatorByCategoryName;


	public static ComparatorById getComparatorById() {
		if (comparatorById == null) {
			comparatorById = new ComparatorById();
		}
		return comparatorById;
	}

	public static Comparator<Product> getComparatorByArticle() {
		if (comparatorByArticle == null) {
			comparatorByArticle = new ComparatorByArticle();
		}
		return comparatorByArticle;
	}

	public static Comparator<Product> getComparatorByCategoryName() {
		if (comparatorByCategoryName == null) {
			comparatorByCategoryName = new ComparatorByCategoryName();
		}
		return comparatorByCategoryName;
	}


	private static class ComparatorById implements Comparator<Product>{
		@Override
		public int compare(Product p1, Product p2) {
			return p1.getId() - p2.getId();
		}
	}

	private static class ComparatorByArticle implements Comparator<Product> {
		@Override
		public int compare(Product p1, Product p2) {
			return p1.getModel() - p2.getModel();
		}
	}

	private static class ComparatorByCategoryName implements Comparator<Product>{
		@Override
		public int compare(Product p1, Product p2) {
			if (p1.getCategoryName() != null && p2.getCategoryName() != null) {
				return p1.getCategoryName().compareTo(p2.getCategoryName());
			}
			return 1;
		}
	}
}
