package edu.sintez.tablesample0.app.util;

import edu.sintez.tablesample0.app.Config;
import edu.sintez.tablesample0.app.model.SQLiteHelper;
import edu.sintez.tablesample0.app.pojo.Product;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

/**
 *
 */
public class CopyImageUtils {

	public void copyImage(Product p, CopyImageAction action) {
		String fileName = p.getFileName();
		String imageName = p.getMasterImage();
		ArrayList<String> additionalImages = p.getAdditionalImages();

		// first image
		if(action == CopyImageAction.COPY_ORIGINAL) copyImageToLocal(imageName, fileName, 0);
		else if (action == CopyImageAction.COPY_WITH_RESIZED) copyImageToLocal(imageName, fileName, 0, false);
		else if (action == CopyImageAction.COPY_WITH_RESIZED_AND_APPLY_WM) copyImageToLocal(imageName, fileName, 0, true);

		// other images
		for (int i = 0; i < additionalImages.size(); i++) {
			if(action == CopyImageAction.COPY_ORIGINAL) copyImageToLocal(additionalImages.get(i), fileName, i + 1);
			else if (action == CopyImageAction.COPY_WITH_RESIZED) copyImageToLocal(additionalImages.get(i), fileName, i + 1, false);
			else if (action == CopyImageAction.COPY_WITH_RESIZED_AND_APPLY_WM) copyImageToLocal(additionalImages.get(i), fileName, i + 1, true);
		}
	}

	/**
	 * Copy tmp image from local, to destination dir without resizing.
	 * This method coping a file from one place to another with his renaming.
	 *
	 * @param imageName   source image name, us only for get file extension
	 * @param newFileName file name for copied file (for created new file)
	 * @param suffix      ending of file, for example if this param = 5 -> fileName_5. Suffix ignored if = 0.
	 */
	private void copyImageToLocal(String imageName, String newFileName, int suffix) {
		String suffixStr = (suffix > 0) ? "_" + String.valueOf(suffix) : "";
		try {
			String ext = Utils.getImageExtension(imageName).getExt();
			Path copy = Files.copy(
				new File(Config.TMP_IMAGE_ABS_PATH + suffixStr + "." + ext).toPath(),
				new File(SQLiteHelper.getInstance().readLocalPathToCopyImgsInSettings()
					+ Utils.replaceForbiddenSymbols(newFileName) + suffixStr + "." + ext).toPath(),
				StandardCopyOption.COPY_ATTRIBUTES
			);
		} catch (FileAlreadyExistsException faee) {
			System.out.println("this file is already exist");
			faee.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Copy tmp image from local, to destination dir with resizing.
	 *
	 * @param imageName
	 * @param newFileName
	 * @param suffix
	 * @param isApplyWaterMark
	 */
	private void copyImageToLocal(String imageName, String newFileName, int suffix, boolean isApplyWaterMark) {
		String suffixStr = (suffix > 0) ? "_" + String.valueOf(suffix) : "";
		String ext = Utils.getImageExtension(imageName).getExt();
		File src = new File(Config.TMP_IMAGE_ABS_PATH + suffixStr + "." + ext);
		System.out.println("src.toString() = " + src.toString());
		File dest = new File(SQLiteHelper.getInstance().readLocalPathToCopyImgsInSettings()
			+ Utils.replaceForbiddenSymbols(newFileName) + suffixStr + "." + ext);
		System.out.println("dest.toString() = " + dest.toString());
		int pixels = Config.RESIZE_IMAGE_RESOLUTION;

		try {
			if (isApplyWaterMark) {
				Thumbnails.of(src)
					.size(pixels, pixels)
					.watermark(Positions.CENTER, ImageIO.read(new File(Config.WATERMARK_PATH)), 1f)
					.outputQuality(0.9)
					.toFile(dest);

			} else {
				Thumbnails.of(src)
					.size(pixels, pixels)
					.outputQuality(0.9)
					.toFile(dest);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public enum CopyImageAction {
		COPY_ORIGINAL, COPY_WITH_RESIZED, COPY_WITH_RESIZED_AND_APPLY_WM
	}

}
