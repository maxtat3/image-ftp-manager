package edu.sintez.tablesample0.app.util;

import edu.sintez.tablesample0.app.pojo.Category;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Utils {

	/**
	 * Check is only number contains in string.
	 * Average performance run time = 0.1 ms.
	 *
	 * @param strNum number in string
	 * @return true - is string contains only digits
	 */
	public static boolean isNumber(final String strNum) {
		if(strNum == null || strNum.isEmpty()) return false;

		ArrayList<Boolean> boolNumbers = new ArrayList<>(strNum.length());
		char[] strChars = strNum.toCharArray();
		for (char c : strChars) {
			if (Character.isDigit(c)) {
				boolNumbers.add(true);
			}
		}
		return strChars.length == boolNumbers.size();
	}

	public static boolean isEmpty(String text) {
		return (text.equals("") || text.equals(" ") || text.equals("  "));
	}

	/**
	 * Create full copy list of {@link Category} objects to new list.
	 *
	 * @param srcList source list
	 * @return new copied list
	 */
	public static List<Category> cloneCategories(List<Category> srcList) {
		List<Category> cloned = new ArrayList<>(srcList.size());
		for (Category cat : srcList) cloned.add(new Category(cat));
		return cloned;
	}



	public static ImageFileType getImageExtension(File file) {
		return fileExt(file.getName());
	}

	public static ImageFileType getImageExtension(String file){
		return fileExt(file);
	}

	private static ImageFileType fileExt(String fileName){
		String ext = "";
		int indexForFullName = fileName.length(); //индекс для полного имени файла включая расширение (например для music.mp3 = 9)
		int indexForDot = fileName.lastIndexOf('.'); //индекс имени файла до разделителя (точка) (например для music.mp3 = 5)

		if (indexForDot > 0) {
			ext = fileName.substring(indexForDot + 1).toLowerCase();
		}

		if (ext.equals(ImageFileType.JPEG.getExt())) {
			return ImageFileType.JPEG;

		} else if (ext.equals(ImageFileType.JPG.getExt())) {
			return ImageFileType.JPG;

		} else if (ext.equals(ImageFileType.PNG.getExt())) {
			return ImageFileType.PNG;

		} else if (ext.equals(ImageFileType.GIF.getExt())) {
			return ImageFileType.GIF;
		}

		return ImageFileType.NOT_DEF;
	}

	public enum ImageFileType {
		JPEG("jpeg"),
		JPG("jpg"), // same as jpeg extension
		PNG("png"),
		GIF("gif"),
		NOT_DEF("");

		private String ext;

		ImageFileType(String ext) {
			this.ext = ext;
		}

		public String getExt() {
			return ext;
		}
	}

	/**
	 * Replacing forbidden symbols in file name.
	 * For more details naming files
	 * <a href="https://msdn.microsoft.com/en-us/library/windows/desktop/aa365247(v=vs.85).aspx">in Windows</a>.
	 * In linux "/" is reserved as it's the directory separator, and "\0" (the NULL character) signify the end of the string.
	 * Everything else symbols is allowed.
	 *
	 * @param text source text
	 * @return cleaned text
	 */
	public static String replaceForbiddenSymbols(String text){
		return text
			.replace(" / ", "_")
			.replace("/", "_")
			.replace("\\", "_")
			.replace("/*", "_")
			.replace("*/", "_")
			.replace(":", "_")
			.replace("*", "x")
			.replace("'", "-")
			.replace("\"", "-");
	}

	/**
	 * Read line from text file.
	 *
	 * @param file path to file
	 * @return reading text
	 */
	public static String readFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			return sb.toString();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void writeToFile(File f, String text){
		try (PrintWriter pw = new PrintWriter(f)) {
			pw.println(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Receive path to dir where this app run.
	 *
	 * @return absolute path
	 */
	public static String receivePathCurrentDir() {
		File file = new File("");
		return file.getAbsolutePath();
	}
}
