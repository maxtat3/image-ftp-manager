package edu.sintez.tablesample0.app.ftp;

import edu.sintez.tablesample0.app.Config;
import edu.sintez.tablesample0.app.util.Utils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Ftp {

	public static final int PORT = 21;

	public String hostingImgPath;
	private FTPClient ftpClient;
	private static Ftp instance = null;


	public static Ftp getInstance() {
		if (instance == null) {
			instance = new Ftp();
		}
		return instance;
	}

	/**
	 * Init FTP connection.
	 *
	 * @return status: true - connection success, otherwise false
	 */
	public boolean init(String host, String userName, String userPsw) {
		ftpClient = new FTPClient();
		try {
			ftpClient.connect(host, PORT);
			showServerReply(ftpClient);
			int replyCode = ftpClient.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode)) {
				System.out.println("Operation failed. Server reply code: " + replyCode);
				return true;
			}
			boolean isLoginSuccess = ftpClient.login(userName, userPsw);
			showServerReply(ftpClient);
			if (!isLoginSuccess) {
				System.out.println("Could not login to the server");
				return true;
			} else {
				System.out.println("LOGGED IN SERVER");
				hostingImgPath = Config.DLM + host + "/image";
			}
		} catch (IOException ex) {
			System.out.println("Oops! Something wrong happened");
			ex.printStackTrace();
		}
		return false;
	}

	/**
	 * See server status.
	 *
	 * @param ftpClient open ftp client
	 */
	private void showServerReply(FTPClient ftpClient) {
		String[] replies = ftpClient.getReplyStrings();
		if (replies != null && replies.length > 0) {
			for (String aReply : replies) {
				System.out.println("SERVER: " + aReply);
			}
		}
	}

	/**
	 * Close connection.
	 *
	 * @throws IOException
	 */
	public void close() {
		if (ftpClient.isConnected()) {
			try {
				ftpClient.logout();
				ftpClient.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Get path to images dir in hosting where stored all images.
	 * Must be call when init process is success.
	 *
	 * @return absolute path to images directory on hosting.
	 */
	public String getHostingImgPath() {
		return hostingImgPath;
	}

	/**
	 * List files and dirs in hosting.
	 *
	 * @return list received paths to file
	 */
	protected List<String> receiveFiles() {
		FTPFile[] hexDirs;
		FTPFile[] imgFiles;
		List<String> dirs = new ArrayList<>(); // !!!
		try {
			hexDirs = ftpClient.listFiles(hostingImgPath);

			for (FTPFile hd : hexDirs) { // image hex named dir, for example: 07, a0, ...
				if (hd.isDirectory()) {
					imgFiles = ftpClient.listFiles(hostingImgPath + Config.DLM + hd.getName());
					System.out.println("dir = " + hostingImgPath + Config.DLM + hd.getName());
//					if (!hd.isDirectory()) {
						for (FTPFile img : imgFiles) {
							System.out.println("file = " + img.getName());
						}
//					}
				}
//                details += "\t\t" + f.getSize();
//                details += "\t\t" + dateFormatter.format(f.getTimestamp().getTime());
//                System.out.println(details);

//                for (String dir : dirs) {
//                    String name = new File(dir).getName();
//                    System.out.println("name = " + name);
//                }
			}

			close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dirs; // !!!
	}

	/**
	 * Receive image file from hosting.
	 * This method download image to local storage and CREATE temp file in local FS where run this app.
	 *
	 * @param remoteFilePath absolute path to image in hosting
	 * @return absolute path to image in local storage (FS)
	 */
	public String downloadFileFromHostingAndSaveToLocal(String remoteFilePath, int suffix) {
		String suffixStr = (suffix > 0) ? "_" + String.valueOf(suffix) : "";
		boolean successRetrieveImg;
		ftpClient.enterLocalPassiveMode();

		try {
			File image = new File(Config.TMP_IMAGE_ABS_PATH);
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			OutputStream os = new BufferedOutputStream(new FileOutputStream(image));
			successRetrieveImg = ftpClient.retrieveFile(remoteFilePath, os);
			os.close();
			if (!successRetrieveImg) return null;

			// rename file - add extension to temp image file
			Path moved = Files.move(
				image.toPath(),
				new File(Config.TMP_IMAGE_ABS_PATH + suffixStr + "." + Utils.getImageExtension(remoteFilePath).getExt()).toPath(),
				StandardCopyOption.REPLACE_EXISTING
			);
			return moved.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
