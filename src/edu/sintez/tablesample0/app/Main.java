package edu.sintez.tablesample0.app;

import edu.sintez.tablesample0.app.ftp.Ftp;
import edu.sintez.tablesample0.app.model.SQLiteHelper;
import edu.sintez.tablesample0.app.pojo.Product;
import edu.sintez.tablesample0.app.util.Utils;
import edu.sintez.tablesample0.app.view.AppFrame;

import javax.swing.*;
import java.util.List;



public class Main {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(true);
	            System.out.println(Utils.receivePathCurrentDir());
//	            SQLiteHelper.getInstance().setDefaultFileNames();
//	            SQLiteHelper.getInstance().setDefaultFileNames(); //tmp

	            // release
	            Ftp.getInstance().init(
		            SQLiteHelper.getInstance().readFtpHostInSettings(),
		            SQLiteHelper.getInstance().readFtpUserNameInSettings(),
		            SQLiteHelper.getInstance().readFtpUserPswInSettings()
	            );
	            SQLiteHelper.getInstance().readProductsIdModelMasterImage();
	            SQLiteHelper.getInstance().readProductAdditionalImages();
	            SQLiteHelper.getInstance().readProductsNameAndFileName();
	            SQLiteHelper.getInstance().bindCategoryToProduct();
//	            if (! SQLiteHelper.getInstance().readTranslationsNamesRusToEngInSettings()) {
//		            if(SQLiteHelper.getInstance().setDefaultFileNames())
//			            SQLiteHelper.getInstance().updateTranslationsNamesRusToEngInSettings(true);
//	            }

	            new AppFrame(SQLiteHelper.getInstance().getProducts());



	            // debug print products
//	            SQLiteHelper.getInstance().readProductsNameAndFileName();
//	            List<Product> products = SQLiteHelper.getInstance().getProducts();
//	            for (Product p : products) {
//		            System.out.println("name=" + p.getName() + "|file name=" + p.getFileName());
//	            }

	            // debug db
//	            SQLiteHelper.getInstance().createAndInitSettings();
//	            SQLiteHelper.getInstance().readTranslationsNamesRusToEngInSettings();
//	            SQLiteHelper.getInstance().updateFtpUserNameInSettings("9435user07563463");
//	            SQLiteHelper.getInstance().updateFtpUserPswInSettings("mypasword7d6f87sd");
//	            SQLiteHelper.getInstance().updateLocalPathToCopyImgsInSettings("path to images/D:dfb/dhh");
//	            SQLiteHelper.getInstance().updatePathToSqliteDbInSettings("path to db : sqlite1.db");
//	            SQLiteHelper.getInstance().updateTranslationsNamesRusToEngInSettings(false);

	            // debug categories query
//	            List<Category> categories = SQLiteHelper.getInstance().readCategories();
//	            for (Category cat : categories) {
//		            System.out.println(cat.getId() + "|" + cat.getName() + "|" + cat.getMetaDescription());
//	            }

	            // debug text cleaned string
//	            String str1 = "abcd / sdljhg/dfg,ndr\\fghft/*dhdth*/htfhf:rgrb*ththf'drignkd\"dlngkjd / dljgtju**dfgdr*drgr****rdg\\fhdhn ";
//	            System.out.println(Utils.replaceForbiddenSymbols(str1));
            }
        });
    }
}
