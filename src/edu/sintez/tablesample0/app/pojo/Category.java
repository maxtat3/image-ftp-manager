package edu.sintez.tablesample0.app.pojo;

/**
 *
 */
public class Category {
	private int id;
	private String name;
	private String metaDescription; // alias

	public Category() {
	}

	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public Category(String name, String metaDescription) {
		this.name = name;
		this.metaDescription = metaDescription;
	}

	/**
	 * Used for full create new object for clone collection.
	 *
	 * @param cat source category
	 */
	public Category(Category cat) {
		this.id = cat.getId();
		this.name = cat.getName();
		this.metaDescription = cat.getMetaDescription();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}
}
