package edu.sintez.tablesample0.app.pojo;

import java.util.ArrayList;

/**
 *
 */
public class Product {

	private int id;
	private int model; //article
	private String name;
	private String fileName; // image file name of this product
	private String masterImage;
	private ArrayList<String> additionalImages = new ArrayList<>(); // n=2,3,...
	private String categoryName;


	public Product() {
	}

	public Product(int id, int model, String name, String fileName, String masterImage, String categoryName) {
		this.id = id;
		this.model = model;
		this.name = name;
		this.fileName = fileName;
		this.masterImage = masterImage;
		this.categoryName = categoryName;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getModel() {
		return model;
	}

	public void setModel(int model) {
		this.model = model;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMasterImage() {
		return masterImage;
	}

	public void setMasterImage(String masterImage) {
		this.masterImage = masterImage;
	}

	public ArrayList<String> getAdditionalImages() {
		return additionalImages;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
